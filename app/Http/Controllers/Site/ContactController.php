<?php

namespace App\Http\Controllers\Site;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\StoreContactRequest;
use App\Models\Contact;
use App\Jobs\SendEmailAdmin;

class ContactController extends Controller
{
    public function index(){
    	return view('site.contact');
    }

    public function store(StoreContactRequest $request){
    	$contact = new Contact;
    	$contact->email = $request->email;
    	$contact->fname = $request->fname;
    	$contact->lname = $request->lname;
    	$contact->save();
    	dispatch(new SendEmailAdmin($request->all()));
    	return redirect()->back()->with('success', 'Your request was sent!');;
    }
}
