<!DOCTYPE html>
<html>
<head>
	<title>Contact us</title>
	<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
	<link rel="stylesheet" type="text/css" href="{{ asset('css/style.css') }}">
</head>
<body>
	<div class="container contact">
		@if(session()->has('success'))
		    <div class="alert alert-success">
		        {{ session()->get('success') }}
		    </div>
		@endif
		<div class="row">
			<div class="col-md-3">
				<div class="contact-info">
					<img src="https://image.ibb.co/kUASdV/contact-image.png" alt="image"/>
					<h2>Contact Us</h2>
					<h4>We would love to hear from you !</h4>
				</div>
			</div>
			<div class="col-md-9">
				<div class="contact-form">
					<form action="{{ route('contact.store') }}" method="post" id="contact-us">
						@csrf
						<div class="form-group">
						  <label class="control-label col-sm-2" for="email">Email:</label>
						  <div class="col-sm-10">
							<input type="email" class="form-control" id="email" required="required" placeholder="Enter email" name="email">
						  </div>
						</div>
						<div class="form-group">
						  <label class="control-label col-sm-2" for="fname">First Name:</label>
						  <div class="col-sm-10">          
							<input type="text" class="form-control" id="fname" required="required" placeholder="Enter First Name" name="fname">
						  </div>
						</div>
						<div class="form-group">
						  <label class="control-label col-sm-2" for="lname">Last Name:</label>
						  <div class="col-sm-10">          
							<input type="text" class="form-control" id="lname" required="required" placeholder="Enter Last Name" name="lname">
						  </div>
						</div>
						<div class="form-group">        
						  <div class="col-sm-offset-2 mt-4 col-sm-10">
							<button type="submit" class="btn btn-default">Submit</button>
						  </div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
	<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
	<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.2/jquery.validate.min.js"></script>
	<script type="text/javascript">
		$("form#contact-us").validate({
			submitHandler: function(form) {
				form.submit();
			}
		});
	</script>
</body>
</html>